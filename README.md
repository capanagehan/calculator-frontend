## Calculator

### What is this app about?

Calculator is a web app. Users can make their calculations and if they log in they can save their calculations.

Check out the website [here](https://calculator-nge.netlify.app/)

If you'd like to play around with the app, you can login with:

- email: a@a.com
- password: a

### Pages overview:

- a home page with a with calculator,
- a signup/login page

### User stories

- As a user I can make basic calculations when I open the homepage.
- As a user I can see my calculation history with expressions and date when I logged in.
- As a user I can signup and login.
- As a user when I login or sign up I am directed to the homepage.
- As a user when I click the "Fiyo" logo, I am directed to the Fiyo's homepage.

### Goals for this project

- demonstrate the main skills I've learned so far about the full stack development,
- build a full-stack web app from stratch,
- planning with user-stories,
- practicing git version control.

### Used technologies

#### Frontend

- React
- Redux
- TypeScript
- Axios

#### Backend

- Node.js
- Express REST API
- JWT & Bcrypt Authentication
- PostgreSQL database
- Sequelize ORM

### Backend repo

Click [here](https://gitlab.com/capanagehan/calculator-backend/) to see the backend repository.
